﻿Imports System.Runtime.CompilerServices
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Security.Cryptography
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Xml.Serialization
Imports System.Windows.Media.Imaging
Imports System.ComponentModel
Imports System.Windows
Imports System.Runtime.InteropServices


Public Module Exts

#Region "Object"

    <System.Runtime.CompilerServices.Extension()> _
    Public Function ToXML(Of T As New)(o As T) As String
        Dim retVal As String
        Using ms = New MemoryStream()
            Dim xs = New XmlSerializer(GetType(T))
            xs.Serialize(ms, o)
            ms.Flush()
            ms.Position = 0
            Dim sr = New StreamReader(ms)
            retVal = sr.ReadToEnd()
        End Using
        Return retVal
    End Function

    <Extension()>
    Public Sub ToXMLFile(Of T As New)(o As T, FilePath As String)
        Dim _x As New XmlSerializer(GetType(T))
        Dim objStreamWriter As New StreamWriter(FilePath)
        _x.Serialize(objStreamWriter, o)
        objStreamWriter.Close()
    End Sub

    <Extension()>
    Public Function FromXMLFile(Of T As New)(FilePath As String) As T
        Dim _x As New XmlSerializer(GetType(T))
        Dim objStreamReader As New StreamReader(FilePath)
        Dim p2 As T = Activator.CreateInstance(GetType(T))
        p2 = _x.Deserialize(objStreamReader)
        objStreamReader.Close()
        Return p2
    End Function

#End Region

#Region "Comparison"

    Public Enum Inclusivity As Integer
        Inclusive = 0
        Exclusive = 1
    End Enum

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="Value"></param>
    ''' <param name="LowerBound"></param>
    ''' <param name="UpperBound"></param>
    ''' <param name="LowerBoundInclusivity"><see cref="SPEM.Between.Inclusivity"/></param>
    ''' <param name="UpperBoundInclusivity"></param>
    ''' 
    ''' <returns><para>
    ''' True 
    ''' False
    ''' </para>
    ''' </returns>
    ''' <remarks></remarks>
    <Extension()>
    Public Function IsBetween(Of T As IComparable)(
                    ByVal Value As T,
                    ByVal LowerBound As T,
                    ByVal UpperBound As T,
                    Optional ByVal LowerBoundInclusivity As Inclusivity = Inclusivity.Inclusive,
                    Optional ByVal UpperBoundInclusivity As Inclusivity = Inclusivity.Inclusive
                    ) As Boolean

        If Not [Enum].IsDefined(GetType(Inclusivity), LowerBoundInclusivity) Then Throw New ArgumentException("")
        If Not [Enum].IsDefined(GetType(Inclusivity), UpperBoundInclusivity) Then Throw New ArgumentException("")
        Return If(LowerBound.CompareTo(UpperBound) <= 0,
                  Value.IsBefore(UpperBound, UpperBoundInclusivity) AndAlso Value.IsAfter(LowerBound, LowerBoundInclusivity),
                  Value.IsBefore(LowerBound, LowerBoundInclusivity) AndAlso Value.IsAfter(UpperBound, UpperBoundInclusivity))
    End Function

    <Extension()>
    Public Function IsAfter(Of T1 As IComparable)(
                                               ByVal Value As T1,
                                               ByVal Pivot As T1,
                                               Optional ByVal PivotInclusivity As Inclusivity = Inclusivity.Inclusive) As Boolean
        If Not [Enum].IsDefined(GetType(Inclusivity), PivotInclusivity) Then Throw New ArgumentException("")
        Return If(PivotInclusivity = Inclusivity.Inclusive, Value.CompareTo(Pivot) >= 0, Value.CompareTo(Pivot) > 0)
    End Function

    <Extension()>
    Public Function IsBefore(Of T1 As IComparable)(
                                               ByVal Value As T1,
                                               ByVal Pivot As T1,
                                               Optional ByVal PivotInclusivity As Inclusivity = Inclusivity.Inclusive) As Boolean
        If Not [Enum].IsDefined(GetType(Inclusivity), PivotInclusivity) Then Throw New ArgumentException("")
        Return If(PivotInclusivity = Inclusivity.Inclusive, Value.CompareTo(Pivot) <= 0, Value.CompareTo(Pivot) < 0)
    End Function

#End Region

#Region "StringBuilder"

    <Extension()>
    Public Function AppendLineFormat(ByVal builder As StringBuilder, ByVal format As String, ByVal ParamArray args As Object()) As StringBuilder
        Dim value As String = String.Format(format, args)

        builder.AppendLine(value)

        Return builder
    End Function

#End Region

#Region "String"
    ''' <summary>
    ''' Attempt to parse input string as double otherwise returns 0#
    ''' </summary>
    ''' <param name="s">String to parse</param>
    ''' <returns>Parsed double if double otherwise 0#</returns>
    ''' <remarks></remarks>
    <Extension()>
    Public Function ToDouble(s As String) As Double
        Dim rtnDbl As Double = 0.0#
        Dim res = Double.TryParse(s, rtnDbl)
        Return rtnDbl
    End Function

    ''' <summary>
    ''' Attempt to parse input string as double otherwise returns 0#
    ''' </summary>
    ''' <param name="s">String to parse</param>
    ''' <returns>Parsed double if double otherwise 0#</returns>
    ''' <remarks></remarks>
    <Extension()>
    Public Function ToDouble(s As String, ValueIfNullorEmpty As Double) As Double
        Dim rtnDbl As Double = ValueIfNullorEmpty
        Dim res = Double.TryParse(s, rtnDbl)
        Return rtnDbl
    End Function

    ''' <summary>
    ''' Attempts to parts input string as integer otherwise returns 0
    ''' </summary>
    ''' <param name="s"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()>
    Public Function ToInt(s As String) As Integer
        Dim rtnInt As Integer = 0
        Dim res = Integer.TryParse(s, rtnInt)
        Return rtnInt
    End Function

    ''' <summary>
    ''' Attempts to parts input string as integer otherwise returns 0
    ''' </summary>
    ''' <param name="s"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()>
    Public Function ToCleanOkumaProgramName(s As String) As String
        Dim rtn As String = ""
        rtn = s.Replace("_", "-").Replace(" ", "").ToUpper()
        If IsNumeric(rtn.Substring(0, 1)) Then rtn = rtn.Insert(0, "A")
        If Not rtn.EndsWith(".MIN") Then
            rtn += ".MIN"
        End If
        If rtn.Length > 15 Then
            rtn = rtn.Substring(0, 10) & ".MIN"
        Else
            rtn = rtn
        End If

        Return rtn

    End Function

    ''' <summary>
    ''' Encryptes a string using the supplied key. Encoding is done using RSA encryption.
    ''' </summary>
    ''' <param name="stringToEncrypt">String that must be encrypted.</param>
    ''' <param name="key">Encryptionkey.</param>
    ''' <returns>A string representing a byte array separated by a minus sign.</returns>
    ''' <exception cref="ArgumentException">Occurs when stringToEncrypt or key is null or empty.</exception>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Encrypt(stringToEncrypt As String, key As String) As String
        If String.IsNullOrEmpty(stringToEncrypt) Then
            Throw New ArgumentException("An empty string value cannot be encrypted.")
        End If

        If String.IsNullOrEmpty(key) Then
            Throw New ArgumentException("Cannot encrypt using an empty key. Please supply an encryption key.")
        End If

        Dim cspp As New CspParameters() With {.KeyContainerName = key}

        Dim rsa As New RSACryptoServiceProvider(cspp) With {.PersistKeyInCsp = True}

        Dim bytes As Byte() = rsa.Encrypt(UTF8Encoding.UTF8.GetBytes(stringToEncrypt), True)

        Return BitConverter.ToString(bytes)
    End Function

    ''' <summary>
    ''' Decryptes a string using the supplied key. Decoding is done using RSA encryption.
    ''' </summary>
    ''' <param name="stringToDecrypt">String that must be decrypted.</param>
    ''' <param name="key">Decryptionkey.</param>
    ''' <returns>The decrypted string or null if decryption failed.</returns>
    ''' <exception cref="ArgumentException">Occurs when stringToDecrypt or key is null or empty.</exception>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Decrypt(stringToDecrypt As String, key As String) As String
        Dim result As String = Nothing

        If String.IsNullOrEmpty(stringToDecrypt) Then
            Throw New ArgumentException("An empty string value cannot be encrypted.")
        End If

        If String.IsNullOrEmpty(key) Then
            Throw New ArgumentException("Cannot decrypt using an empty key. Please supply a decryption key.")
        End If

        Try
            Dim cspp As New CspParameters() With {.KeyContainerName = key}

            Dim rsa As New RSACryptoServiceProvider(cspp) With {.PersistKeyInCsp = True}

            Dim decryptArray As String() = stringToDecrypt.Split(New String() {"-"}, StringSplitOptions.None)
            Dim decryptByteArray As Byte() = Array.ConvertAll(Of String, Byte)(decryptArray,
                                                                               (Function(s) Convert.ToByte(
                                                                                    Byte.Parse(s, System.Globalization.NumberStyles.HexNumber))))


            Dim bytes As Byte() = rsa.Decrypt(decryptByteArray, True)


            result = UTF8Encoding.UTF8.GetString(bytes)
            ' no need for further processing
        Finally
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Returns the first few characters of the string with a length
    ''' specified by the given parameter. If the string's length is less than the 
    ''' given length the complete string is returned. If length is zero or 
    ''' less an empty string is returned
    ''' </summary>
    ''' <param name="s">the string to process</param>
    ''' <param name="length">Number of characters to return</param>
    ''' <returns></returns>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Left(s As String, length As Integer) As String
        length = Math.Max(length, 0)

        If s.Length > length Then
            Return s.Substring(0, length)
        Else
            Return s
        End If
    End Function

    ''' <summary>
    ''' Returns the last few characters of the string with a length
    ''' specified by the given parameter. If the string's length is less than the 
    ''' given length the complete string is returned. If length is zero or 
    ''' less an empty string is returned
    ''' </summary>
    ''' <param name="s">the string to process</param>
    ''' <param name="length">Number of characters to return</param>
    ''' <returns></returns>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Right(s As String, length As Integer) As String
        length = Math.Max(length, 0)

        If s.Length > length Then
            Return s.Substring(s.Length - length, length)
        Else
            Return s
        End If
    End Function

    ''' <summary>
    ''' Strip a string of the specified character.
    ''' </summary>
    ''' <param name="s">the string to process</param>
    ''' <param name="char">character to remove from the string</param>
    ''' <example>
    ''' string s = "abcde";
    ''' 
    ''' s = s.Strip('b');  //s becomes 'acde;
    ''' </example>
    ''' <returns></returns>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Strip(s As String, character As Char) As String
        s = s.Replace(character.ToString(), "")

        Return s
    End Function

    ''' <summary>
    ''' Strip a string of the specified characters.
    ''' </summary>
    ''' <param name="s">the string to process</param>
    ''' <param name="chars">list of characters to remove from the string</param>
    ''' <example>
    ''' string s = "abcde";
    ''' 
    ''' s = s.Strip('a', 'd');  //s becomes 'bce;
    ''' </example>
    ''' <returns></returns>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Strip(s As String, ParamArray chars As Char()) As String
        For Each c As Char In chars
            s = s.Replace(c.ToString(), "")
        Next

        Return s
    End Function

    ''' <summary>
    ''' Strip a string of the specified substring.
    ''' </summary>
    ''' <param name="s">the string to process</param>
    ''' <param name="subString">substring to remove</param>
    ''' <example>
    ''' string s = "abcde";
    ''' 
    ''' s = s.Strip("bcd");  //s becomes 'ae;
    ''' </example>
    ''' <returns></returns>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function Strip(s As String, subString As String) As String
        s = s.Replace(subString, "")

        Return s
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function IsValidEmailAddress(s As String) As Boolean
        Dim regex As New Regex("^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")
        Return regex.IsMatch(s)
    End Function

    <Extension()>
    Public Function IsNullOrEmpty(input As String) As Boolean
        Return If(String.IsNullOrEmpty(input), True, False)
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function IsNotNullOrEmpty(input As String) As Boolean
        Return Not [String].IsNullOrEmpty(input)
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function IsValidIPAddress(s As String) As Boolean
        Return Regex.IsMatch(s, "\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b")
    End Function

    <System.Runtime.CompilerServices.Extension()> _
    Public Function Reverse(s As String) As String
        Dim c As Char() = s.ToCharArray()
        Array.Reverse(c)
        Return New String(c)
    End Function

    Public Function CountLines(reader As TextReader) As Integer
        Dim buffer As Char() = New Char(32 * 1024 - 1) {}
        ' Read 32K chars at a time
        Dim total As Integer = 1
        ' All files have at least one line!
        Dim read As Integer
        While (InlineAssignHelper(read, reader.Read(buffer, 0, buffer.Length))) > 0
            For i As Integer = 0 To read - 1
                If buffer(i) = ControlChars.Lf Then
                    total += 1
                End If
            Next
        End While
        Return total
    End Function

    Private Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
        target = value
        Return value
    End Function

    <Extension()>
    Public Function GetListFromXMLString(Of T)(ByVal XML As String) As List(Of T)
        If String.IsNullOrEmpty(XML) Then
            Throw New ArgumentNullException("XML")
        End If
        Dim serializer As New XmlSerializer(GetType(List(Of T)))
        Using stream As New StringReader(XML)
            Try
                Return DirectCast(serializer.Deserialize(stream), List(Of T))
            Catch ex As Exception
                Throw New InvalidOperationException("Failed to " & "create object from xml string", ex)
            End Try
        End Using
    End Function




#End Region

#Region "Double"



#End Region

#Region "Lists"

    'http://blogs.microsoft.co.il/blogs/kim/archive/2006/05/23/SerializationWithDotNet2Generics.aspx
    'http://mytipsfordotnet.blogspot.com/2011/04/how-to-serialize-and-deserialize.html
    <Extension()>
    Public Function GetXML(Of T)(ByVal ObjList As List(Of T)) As String
        Dim serializer As New XmlSerializer(ObjList.GetType())
        Using Stream As New StringWriter()
            serializer.Serialize(Stream, ObjList)
            Stream.Flush()
            Return Stream.ToString
        End Using
    End Function

    ''' <summary>
    ''' Determines whether the specified collection has any elements in the sequence.
    ''' This method also checks for a null collection.
    ''' </summary>
    ''' <param name="items">The ICollection of items to check.</param>
    <Extension()> _
    Public Function HasElements(items As ICollection) As Boolean
        Return items IsNot Nothing AndAlso items.Count > 0
    End Function

    ''' <summary>
    ''' Adds an object to the list only if it does not already exist in the list.
    ''' </summary>
    ''' <param name="ls"></param>
    ''' <param name="value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Extension()> _
    Public Function AddIfDoesntExist(Of T)(ByVal ls As List(Of T), ByVal value As T) As List(Of T)
        'http://www.extensionmethod.net/Details.aspx?ID=285
        If ls.Contains(value) = False Then
            ls.Add(value)
        End If

        Return ls
    End Function

#End Region

#Region "ICollection"

#End Region

#Region "Integer"

    <Extension()>
    Public Function GetBitBoolValue(ByVal value As Integer, ByVal pos As Integer) As Boolean
        Return ((value And (1 << pos)) > 0)
    End Function

    <Extension()>
    Public Function GetBitValue(ByVal value As Integer, ByVal pos As Integer) As Integer
        Return If(GetBitBoolValue(value, pos), 1, 0)
    End Function

    <Extension()>
    Public Function SetBitBoolValue(value As Integer, pos As Integer, isBitOn As Boolean) As Integer
        Dim num = (1 << pos)
        Dim num2 = (num Or value)
        num2 = (num2 - num)
        num = (If((isBitOn), 1, 0)) << pos
        Return num2 + num
    End Function

    <Extension()>
    Public Function SetBitValue(ByVal value As Integer, ByVal pos As Integer, ByVal bitValue As Integer) As Integer
        Return SetBitBoolValue(value, pos, (bitValue > 0))
    End Function

#End Region

#Region "UInteger"
    <Extension()>
    Public Function GetBitBoolValue(ByVal value As UInteger, ByVal pos As Integer) As Boolean
        Return ((value And (1 << pos)) > 0)
    End Function

    <Extension()>
    Public Function GetBitValue(ByVal value As UInteger, ByVal pos As Integer) As UInteger
        Return If(GetBitBoolValue(value, pos), 1, 0)
    End Function

    <Extension()>
    Public Function SetBitBoolValue(value As UInteger, pos As Integer, isBitOn As Boolean) As UInteger
        Dim num = (1 << pos)
        Dim num2 = (num Or value)
        num2 = (num2 - num)
        num = (If((isBitOn), 1, 0)) << pos
        Return num2 + num
    End Function

    <Extension()>
    Public Function SetBitValue(ByVal value As UInteger, ByVal pos As Integer, ByVal bitValue As Integer) As UInteger
        Return SetBitBoolValue(value, pos, (bitValue > 0))
    End Function


#End Region

#Region "Byte"

    <System.Runtime.CompilerServices.Extension()> _
    Public Function ToMemoryStream(buffer As [Byte]()) As MemoryStream
        Dim ms As New MemoryStream(buffer) With {.Position = 0}
        Return ms
    End Function

    <Extension()>
    Public Function GetBitBoolValue(ByVal value As Byte, ByVal pos As Byte) As Boolean
        If (pos > 7) Then
            Throw New ArgumentOutOfRangeException("Pos", pos, "The value should range from 0 to 7")
        End If
        Return ((value And (1 << pos)) > 0)
    End Function

    <Extension()>
    Public Function GetBitValue(ByVal value As Byte, ByVal pos As Byte) As Byte
        If (pos > 7) Then
            Throw New ArgumentOutOfRangeException("Pos", pos, "The value should range from 0 to 7")
        End If
        Return If(GetBitBoolValue(value, pos), 1, 0)
    End Function

    <Extension()>
    Public Function SetBitBoolValue(value As Byte, pos As Byte, isBitOn As Boolean) As Byte
        If (pos > 7) Then
            Throw New ArgumentOutOfRangeException("Pos", pos, "The value should range from 0 to 7")
        End If
        Dim num = (1 << pos)
        Dim num2 = (num Or value)
        num2 = (num2 - num)
        num = (If((isBitOn), 1, 0)) << pos
        Return num2 + num
    End Function

    <Extension()>
    Public Function SetBitValue(ByVal value As Byte, ByVal pos As Byte, ByVal bitValue As Byte) As Byte
        If (pos > 7) Then
            Throw New ArgumentOutOfRangeException("Pos", pos, "The value should range from 0 to 7")
        End If
        Return SetBitBoolValue(value, pos, (bitValue > 0))
    End Function

#End Region

#Region "Enums"

    <Extension()> _
    Public Function TryParse(Of T)(theEnum As [Enum], strType As String, result As T) As Boolean
        Dim strTypeFixed As String = strType.Replace(" "c, "_"c)
        If [Enum].IsDefined(GetType(T), strTypeFixed) Then
            result = DirectCast([Enum].Parse(GetType(T), strTypeFixed, True), T)
            Return True
        Else
            For Each value As String In [Enum].GetNames(GetType(T))
                If value.Equals(strTypeFixed, StringComparison.OrdinalIgnoreCase) Then
                    result = DirectCast([Enum].Parse(GetType(T), value), T)
                    Return True
                End If
            Next
            result = Nothing
            Return False
        End If
    End Function

    <Extension()> _
    Public Function IsItemInEnum(Of TEnum As Structure)(dataToCheck As String) As Func(Of Boolean)
        Return Function()
                   Return String.IsNullOrEmpty(dataToCheck) OrElse Not [Enum].IsDefined(GetType(TEnum), dataToCheck)
               End Function
    End Function

    '        <Extension()>
    '        Public Function Parse(Of t)(value As String) As T
    '            Return [Enum](Of T).Parse(value, True)
    '        End Function
    '        <Extension()>
    '        Public Function Parse(value As String, ignoreCase As Boolean) As T
    '            Return DirectCast([Enum].Parse(GetType(T), value, ignoreCase), T)
    '        End Function
    '        <Extension()>
    '        Public Function TryParse(value As String, returnedValue As T) As Boolean
    '            Return Enum(Of T).TryParse(value, True, returnedValue)
    '        End Function
    '        <Extension()>
    '        Public Function TryParse(value As String, ignoreCase As Boolean, returnedValue As T) As Boolean
    '            Try
    '                returnedValue = DirectCast([Enum].Parse(GetType(T), value, ignoreCase), T)
    '                Return True
    '            Catch
    '                returnedValue = Nothing
    '                Return False
    '            End Try
    '        End Function

#End Region

#Region "Exception.ToLogString"
    ''' <summary>
    ''' <para>Creates a log-string from the Exception.</para>
    ''' <para>The result includes the stacktrace, innerexception et cetera, separated by <seealso cref="Environment.NewLine"/>.</para>
    ''' </summary>
    ''' <param name="ex">The exception to create the string from.</param>
    ''' <param name="additionalMessage">Additional message to place at the top of the string, maybe be empty or null.</param>
    ''' <returns></returns>
    <System.Runtime.CompilerServices.Extension()> _
    Public Function ToLogString(ex As Exception, additionalMessage As String) As String
        Dim msg As New StringBuilder()

        If Not String.IsNullOrEmpty(additionalMessage) Then
            msg.Append(additionalMessage)
            msg.Append(Environment.NewLine)
        End If

        If ex IsNot Nothing Then
            Try
                Dim orgEx As Exception = ex

                msg.Append("Exception:")
                msg.Append(Environment.NewLine)
                While orgEx IsNot Nothing
                    msg.Append(orgEx.Message)
                    msg.Append(Environment.NewLine)
                    orgEx = orgEx.InnerException
                End While

                If ex.Data IsNot Nothing Then
                    For Each i As Object In ex.Data
                        msg.Append("Data :")
                        msg.Append(i.ToString())
                        msg.Append(Environment.NewLine)
                    Next
                End If

                If ex.StackTrace IsNot Nothing Then
                    msg.Append("StackTrace:")
                    msg.Append(Environment.NewLine)
                    msg.Append(ex.StackTrace.ToString())
                    msg.Append(Environment.NewLine)
                End If

                If ex.Source IsNot Nothing Then
                    msg.Append("Source:")
                    msg.Append(Environment.NewLine)
                    msg.Append(ex.Source)
                    msg.Append(Environment.NewLine)
                End If

                If ex.TargetSite IsNot Nothing Then
                    msg.Append("TargetSite:")
                    msg.Append(Environment.NewLine)
                    msg.Append(ex.TargetSite.ToString())
                    msg.Append(Environment.NewLine)
                End If

                Dim baseException As Exception = ex.GetBaseException()
                If baseException IsNot Nothing Then
                    msg.Append("BaseException:")
                    msg.Append(Environment.NewLine)
                    msg.Append(ex.GetBaseException())
                End If
            Finally
            End Try
        End If
        Return msg.ToString()
    End Function
#End Region

#Region "Regex"
    <System.Runtime.CompilerServices.Extension> _
    Public Function MatchNamedCaptures(regex As Regex, input As String) As Dictionary(Of String, String)
        Dim namedCaptureDictionary = New Dictionary(Of String, String)()
        Dim groups As GroupCollection = regex.Match(input).Groups
        Dim groupNames As String() = regex.GetGroupNames()
        For Each groupName As String In groupNames
            If groups(groupName).Captures.Count > 0 Then
                namedCaptureDictionary.Add(groupName, groups(groupName).Value)
            End If
        Next
        Return namedCaptureDictionary
    End Function

#End Region

End Module


