﻿Imports System.IO

Module Module1

    Sub Main()
        Try

            Dim rpt = ""
            While rpt.ToLower <> "q"

                        RunDirect()

                Console.WriteLine("Complete! Push Enter to continue or Q to quit")

                rpt = Console.ReadLine()

            End While

        Catch ex As Exception
            Console.WriteLine(ex.ToLogString(""))
        End Try


    End Sub

    Private Sub RunDirect()
        Dim okuma = New Okuma.CMDATAPI.DataAPI.CMachine
        okuma.Init()

        Dim atc = New Okuma.CMDATAPI.DataAPI.CATC


        For i As Integer = 1 To atc.GetMaxPots
            Console.WriteLine("Reading the tool in Pot {0}", i)

            Dim tool = 0
            Dim tprop As Okuma.CMDATAPI.Structures.ToolProperty = Nothing
            Dim tpropString = ""
            Try
                tprop = atc.GetPotTool(i)
                tpropString = String.Format("intToolNo:{0} | strToolKind:{1}", tprop.intToolNo.ToString, tprop.strToolKind.ToString)
                tool = tprop.intToolNo

            Catch ex As Exception
                Dim msg = String.Format("Problem getting the tool number or pot {0}. {1}Tool property details:{3}{1} Exception details below:{1}{1}:{2}", i, vbCrLf, ex.ToLogString(""), tpropString)
                Console.WriteLine(msg)
                File.WriteAllText("ErrorDirect.log", msg)
                Console.ReadLine()
            End Try

            Console.WriteLine("The Tool in Pot {0} is {1}", i, tool)
            Console.WriteLine()

        Next

        okuma.Close()

    End Sub


End Module
